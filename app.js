import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

import routes from './routes/index';

const app = express();

/**
 *  Connect to database
 */
mongoose.connect('mongodb://localhost');

/**
 *  Middleware
 */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set header
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3001');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

// Catch 400
app.use((err, req, res, next) => {
    console.log(err.stack);

    res.status(400).send(`Error: ${ res.originalUrl } not found`);
    next();
});

// Catch 500
app.use((err, req, res, next) => {
    console.log(err.stack);

    res.status(500).send(`Error: ${ err }`);
    next();
});

routes(app);

export default app;